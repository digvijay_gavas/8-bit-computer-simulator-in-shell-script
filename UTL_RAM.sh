# RAM I/O related functions 
# this file need RAM file as input
FILE_RAM=$1
BASE=16

fRAM_getDataAt()
{
	grep  "^[0]*$1 " $FILE_RAM | sed "s|^[0]*$1 ||g"
}

fRAM_setDataAt()
{
	sed -i "s|^[0]*$1 .*$|$1 $2|g" $FILE_RAM
}



fRAM_reset()
{
	echo "Resetted on $date" >$FILE_RAM
	for (( i=0; i<256 ; i++ ))
	{
		echo $(echo "obase=$BASE;$i; 00 " | bc | tr '\n' ' ' ; echo;)>>$FILE_RAM
	}
}
