FILE_REGISTER=$1
BASE=16

fREGISTER_getDataAt()
{
	grep  "^[0]*$1 " $FILE_REGISTER | sed "s|^[0]*$1 ||g"
}

fREGISTER_setDataAt()
{
	sed -i "s|^[0]*$1 .*$|$1 $2|g" $FILE_REGISTER
}



fREGISTER_reset()
{
	#echo "Resetted on $(date)" >$FILE_REGISTER
	sed -i "s| [0-9A-F]*$| 00|g" $FILE_REGISTER
}
