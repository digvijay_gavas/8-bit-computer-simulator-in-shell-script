# All instruction are coded here
# B[0-F] is the format for defining function name of instruction,
# followed by actual function defination
# e.g.  B1 stand for I_LDA means Instruction load A and is assigned to 0001 binary code
# there are 16 diffrent instructions possible, out of which 8 only defined here

 
B1=I_LDA
I_LDA()
{
	fREGISTER_setDataAt RA $1
}

B2=I_LDB
I_LDB()
{
        fREGISTER_setDataAt RB $1
}

B3=I_ADD
I_ADD()
{
	fREGISTER_setDataAt RA $(fI_DecToHex $(( 0x$(fREGISTER_getDataAt RB) + 0x$(fREGISTER_getDataAt RA) )))
}

B4=I_OUT
I_OUT()
{
	fREGISTER_setDataAt OUT $(fREGISTER_getDataAt RA) 
	fDISPLAY_send $(fREGISTER_getDataAt RA) $1
}

B5=I_LAM
I_LAM()
{
	fREGISTER_setDataAt RA $(fRAM_getDataAt $1)
}

B6=I_LBM
I_LBM()
{
        fREGISTER_setDataAt RB $(fRAM_getDataAt $1)
}


B7=I_WAM
I_WAM()
{
	fRAM_setDataAt $1 $(fREGISTER_getDataAt RA)
}

B8=I_WBM
I_WBM()
{
	fRAM_setDataAt $1 $(fREGISTER_getDataAt RB)
}

BB=I_CAB
I_CAB()
{
        fREGISTER_setDataAt RJ $(fI_DecToHex $(( 0x$(fREGISTER_getDataAt RB) - 0x$(fREGISTER_getDataAt RA) )))
}


BC=I_CMP
I_CMP()
{
	fREGISTER_setDataAt RJ $(fI_DecToHex $(( 0x${1} - 0x$(fREGISTER_getDataAt RA) )) )

}


BD=I_JIF
I_JIF()
{
	if (( 0x$(fREGISTER_getDataAt RJ) > 0 ))
	then
		fCTRL_setProgramCounter $((0x$1 -1))
	fi
}

BE=I_JMP
I_JMP()
{
	fCTRL_setProgramCounter $((0x$1 -1))
}

BF=I_HLT
I_HLT()
{
	fCTRL_halt
}


# decode instructions to shell function with argument
# output of this can be directly executed to perform intruction
 
fI_decodeIntruction()
{
	echo $(eval echo "\$B${1:0:1}") ${1:1}
}


fI_DecToHex()
{
	echo "obase=$BASE;$1" | bc
}

