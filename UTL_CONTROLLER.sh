# All Controller related functions are defined here
# the default propgram counter point to first position in RAM
PROGRAM_COUNTER=01
CURRENT_INTRUCTION=HLT
COMPUTER_HALT=0


fCTRL_advanceProgramCounter()
{
	((PROGRAM_COUNTER++))
}

fCTRL_setProgramCounter()
{
	PROGRAM_COUNTER=$1
}

fCTRL_getProgramCounter()
{
	echo $PROGRAM_COUNTER
}

fCTRL_halt()
{
	COMPUTER_HALT=1
}

fCTRL_stopBreakOnHalt()
{
	if [ $COMPUTER_HALT -ne 0 ]; then break; fi
}
fCTRL_reset()
{
	while [ $COMPUTER_HALT -eq 0 ]
	do
		fCTRL_advanceIntruction
	done
}
fCTRL_setIntruction()
{
	CURRENT_INTRUCTION="$(fI_decodeIntruction $1 )"
	echo CURRENT_INTRUCTION=$CURRENT_INTRUCTION
}
fCTRL_advanceIntruction()
{
	fCTRL_setIntruction $(fRAM_getDataAt $(echo "obase=$BASE;$PROGRAM_COUNTER;" | bc))
	$CURRENT_INTRUCTION
	fCTRL_advanceProgramCounter
}
