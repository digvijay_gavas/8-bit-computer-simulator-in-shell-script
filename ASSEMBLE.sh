# this file will convert your assembly file (.assembly) to machine readable format (.machine) 
# give first argument as .assembly file and second argument as .machine file
INPUT_FILE=/dev/stdin
if [ -f "$1" ]
then
	INPUT_FILE="$1"	
fi

OUTPUT_FILE=/dev/stdout
if [ ! -z "$2" ]
then
        OUTPUT_FILE="$2"     
	rm $OUTPUT_FILE 
fi
sed_command="
s|^I_||
$(sed -n  "s|^B\([0-9A-F]\)=I_\([A-Z][A-Z][A-Z]\)$|s\|\^\1\$\|\2\||p" UTL_INSTRUCTIONS.sh)"

while read counter hex
do
	instruction="${hex:0:1}"
	argument="${hex:1:2}"
	echo "$counter $(echo $instruction |sed "$sed_command") $argument" >>$OUTPUT_FILE
done <"$INPUT_FILE"
